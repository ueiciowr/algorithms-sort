package com;

import java.util.Random;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int tam = 10;
        int[] arr = new int[tam];
        insereValores(arr);
        int len = arr.length;

        Scanner scan = new Scanner(System.in);
        System.out.print(
                "algoritmo deseja executar ?\n1) Insertion\n2) Selection\n3) Bubble\n4) Shell\n5) Quick\n6) Heap\n7) Merge\nInsira o número: ");
        int key = scan.nextInt();
        scan.close();

        switch (key) {
            case 1:
                InsertionSort(arr, len);
                break;
            case 2:
                SelectionSort(arr, len);
                break;
            case 3:
                BubbleSort(arr, len);
                break;
            case 4:
                ShellSort(arr, len);
                break;
            case 5:
                quicksort(arr, 0, len - 1);
                break;
            case 6:
                heapSort(arr, len);
                break;
            case 7:
                mergeSort(0, arr.length - 1, arr);
                break;
            default:
                break;
        }

        System.out.println("===== Vetor Ordenado =====");
        imprimeValores(arr);
    }

    static void insereValores(int[] v) {
        Random gerador = new Random();
        int n = v.length;
        for (int i = 0; i < n; i++) {
            v[i] = gerador.nextInt(100);
        }
    }

    static void imprimeValores(int[] arr) {
        for (int j = 0; j < arr.length; j++)
            System.out.printf("[%d] = %2d\n", j, arr[j]);
    }

    // + ---------- Insertionsort ------------------- +
    static void InsertionSort(int[] v, int n) {
        for (int i = 1; i < n; ++i) {
            int key = v[i];
            int j = i - 1;
            while (j >= 0 && v[j] > key) {
                v[j + 1] = v[j];
                j = j - 1;
            }
            v[j + 1] = key;
        }
    }

    // + ---------- Selectionsort ------------------- +
    static void SelectionSort(int[] v, int n) {
        int i, j, menor_idx, aux;
        for (i = 0; i < n - 1; i++) {
            menor_idx = i;
            for (j = i + 1; j < n; j++)
                if (v[j] < v[menor_idx]) {
                    menor_idx = j;
                }
            aux = v[menor_idx];
            v[menor_idx] = v[i];
            v[i] = aux;
        }
    }

    // + ------------ Bubblesort -------------------- +
    static void BubbleSort(int[] v, int n) {
        int i, j, aux;
        for (i = n - 1; i > 0; i--) {
            for (j = 0; j < i; j++) {
                System.out.printf("==== Passo(%d) i=%d, j=%d ==== \n", i, i, j);
                imprimeValores(v);
                if (v[j] > v[j + 1]) {
                    aux = v[j];
                    v[j] = v[j + 1];
                    v[j + 1] = aux;
                }
            }
        }
    }

    // + ------------ Shellsort --------------------- +
    static void ShellSort(int[] arr, int len) {
        for (int gap = len / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < len; i += 1) {
                int temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
    }

    // + ------------ Quicksort --------------------- +
    static int quickPartition(int[] arr, int low, int high) {
        int pivot = arr[high];
        int i = low - 1;
        for (int j = low; j < high; j++) {
            if (arr[j] < pivot) {
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;
        return (i + 1);
    }

    static void quicksort(int[] arr, int low, int high) {
        if (low < high) {
            int pi = quickPartition(arr, low, high);
            quicksort(arr, low, pi - 1);
            quicksort(arr, pi + 1, high);
        }
    }

    // + ------------ Heapsort ---------------------- +
    static void heapify(int[] arr, int len, int i) {
        int leftIdx = 2 * i + 1, rightIdx = 2 * i + 2, big = i, temp = 0;
        if (leftIdx < len && arr[leftIdx] > arr[big]) {
            big = leftIdx;
        }
        if (rightIdx < len && arr[rightIdx] > arr[big]) {
            big = rightIdx;
        }
        if (big != i) {
            temp = arr[i];
            arr[i] = arr[big];
            arr[big] = temp;
            heapify(arr, len, big);
        }
    }

    static void heapSort(int[] arr, int len) {
        int i = 0;
        for (i = len / 2 - 1; i >= 0; i--) {
            heapify(arr, len, i);
        }
        for (i = len - 1; i >= 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, i, 0);
        }
    }

    // + ------------ Heapsort ---------------------- +
    static void merge(int p, int q, int len, int[] arr) {
        int n1 = q - p + 1;
        int n2 = len - q;
        int L[] = new int[n1];
        int M[] = new int[n2];

        for (int i = 0; i < n1; i++)
            L[i] = arr[p + i];
        for (int j = 0; j < n2; j++)
            M[j] = arr[q + 1 + j];
        int i = 0, j = 0, k = p;
        while (i < n1 && j < n2) {
            if (L[i] <= M[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = M[j];
                j++;
            }
            k++;
        }
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }
        while (j < n2) {
            arr[k] = M[j];
            j++;
            k++;
        }
    }

    static void mergeSort(int left, int right, int[] arr) {
        if (left < right) {
            int merge = (left + right) / 2;
            mergeSort(left, merge, arr);
            mergeSort(merge + 1, right, arr);
            merge(left, merge, right, arr);
        }
    }
}
