package com.person;

import java.util.Formatter;

public class Contatos {
  private Pessoa[] vc; // vetor de contatos
  private int numContatos; // número de contatos

  public Contatos(int tamanho) {
    this.vc = new Pessoa[tamanho];
    this.numContatos = 0;
  }

  public Pessoa[] getPerson() {
    return vc;
  }

  public int getNumContatos() {
    return this.numContatos;
  }

  public void inserePessoa(int id, String nome, String sobrenome, int idade) {
    this.vc[numContatos] = new Pessoa(id, nome, sobrenome, idade);
    this.numContatos++;
  }

  public void imprimeContatos(double time, int result) throws Exception {
    try {
      Formatter save = new Formatter(
          "C:\\Users\\Mauricio\\Desktop\\ESTUDOS\\algorithms-sort-gitlab\\2\\orderListAlgorithm.txt");
      for (int i = 0; i < this.numContatos; i++) {
        save.format("%d %s %s %d\n", vc[i].getId(), vc[i].getNome(), vc[i].getSobrenome(), vc[i].getIdade());
      }
      save.format("\nTempo de Ordenação %.3f ms%n", time);
      String r = getStatus(result);
      save.format("Ordenação por %s", r);
      save.close();
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public String getStatus(int result) {
    if (result == 1)
      return "InsertionSort";
    else if (result == 2)
      return "BubbleSort";
    else if (result == 3)
      return "SelectionSort";
    else if (result == 4)
      return "ShellSort";
    else if (result == 5)
      return "QuickSort";
    else if (result == 6)
      return "HeapSort";
    else if (result == 7)
      return "MergeSort";
    return "Forma de ordenação não encontrada!";
  }

  // + ------------------------------------------------------------- +
  // + INSERTION SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int insertionSort(String algorithm) {
    int length = this.numContatos;
    if (algorithm.equals("name")) {
      for (int i = 1; i < length; ++i) {
        Pessoa key = vc[i];
        int j = i - 1;
        while (j >= 0 && vc[j].getNome().compareTo(key.getNome()) > 0) {
          vc[j + 1] = vc[j];
          j = j - 1;
        }
        vc[j + 1] = key;
      }
    } else if (algorithm.equals("surname")) {
      for (int i = 1; i < length; i++) {
        Pessoa key = vc[i];
        int j = i - 1;
        while (j >= 0 && vc[j].getSobrenome().compareTo(key.getSobrenome()) > 0) {
          vc[j + 1] = vc[j];
          j--;
        }
        vc[j + 1] = key;
      }
    } else if (algorithm == "" || algorithm.equals("age")) {
      for (int i = 1; i < length; i++) {
        Pessoa key = vc[i];
        int j = i - 1;
        while (j >= 0 && vc[j].getIdade() > key.getIdade()) {
          vc[j + 1] = vc[j];
          j--;
        }
        vc[j + 1] = key;
      }
    } else {
      return 0;
    }
    return 1;
  }

  // + ------------------------------------------------------------- +
  // + BUBBLE SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int bubbleSort(String algorithm) {
    Pessoa aux;
    if (algorithm.equals("id")) {
      for (int i = this.numContatos - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
          if (vc[j].getId() > vc[j + 1].getId()) {
            aux = vc[j];
            vc[j] = vc[j + 1];
            vc[j + 1] = aux;
          }
        }
      }
    } else if (algorithm.equals("name")) {
      for (int i = this.numContatos - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
          if (vc[j].getNome().compareTo(vc[j + 1].getNome()) > 0) {
            aux = vc[j];
            vc[j] = vc[j + 1];
            vc[j + 1] = aux;
          }
        }
      }
    } else if (algorithm.equals("surname")) {
      for (int i = this.numContatos - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
          if (vc[j].getSobrenome().compareTo(vc[j + 1].getSobrenome()) > 0) {
            aux = vc[j];
            vc[j] = vc[j + 1];
            vc[j + 1] = aux;
          }
        }
      }
    } else if (algorithm == "" || algorithm.equals("age")) {
      for (int i = this.numContatos - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
          if (vc[j].getIdade() > vc[j + 1].getIdade()) {
            aux = vc[j];
            vc[j] = vc[j + 1];
            vc[j + 1] = aux;
          }
        }
      }
    } else {
      return 0;
    }
    return 2;
  }

  public void bubbleSortFirstNameAdapted() {
    int firstIndex, secondIndex;
    Pessoa aux;
    for (firstIndex = this.numContatos - 1; firstIndex > 0; firstIndex--) {
      for (secondIndex = 0; secondIndex < firstIndex; secondIndex++) {
        if (vc[firstIndex].getNome().compareTo(vc[secondIndex].getNome()) < 0) {
          aux = vc[firstIndex];
          vc[firstIndex] = vc[secondIndex];
          vc[secondIndex] = aux;
        }
      }
    }
  }

  // + ------------------------------------------------------------- +
  // + SELECTION SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int selectionSort(String algorithm) {
    int menorIndex = 0, length = this.numContatos;
    Pessoa aux;
    if (algorithm.equals("age")) {
      for (int i = 0; i < length - 1; i++) {
        menorIndex = i;
        for (int j = i; j < length; j++) {
          if (vc[j].getIdade() < vc[menorIndex].getIdade()) {
            menorIndex = j;
          }
        }
        aux = vc[menorIndex];
        vc[menorIndex] = vc[i];
        vc[i] = aux;
      }
    } else if (algorithm.equals("name")) {
      for (int i = 0; i < length - 1; i++) {
        menorIndex = i;
        for (int j = i; j < length; j++) {
          if (vc[j].getNome().compareTo(vc[menorIndex].getNome()) < 0) {
            menorIndex = j;
          }
        }
        aux = vc[menorIndex];
        vc[menorIndex] = vc[i];
        vc[i] = aux;
      }
    } else if (algorithm.equals("surname")) {
      for (int i = 0; i < length - 1; i++) {
        menorIndex = i;
        for (int j = i; j < length; j++) {
          if (vc[j].getSobrenome().compareTo(vc[menorIndex].getSobrenome()) < 0) {
            menorIndex = j;
          }
        }
        aux = vc[menorIndex];
        vc[menorIndex] = vc[i];
        vc[i] = aux;
      }
    } else if (algorithm == "" || algorithm.equals("id")) {
      for (int i = 0; i < length - 1; i++) {
        menorIndex = i;
        for (int j = i; j < length; j++) {
          if (vc[j].getId() < vc[menorIndex].getId()) {
            menorIndex = j;
          }
        }
        aux = vc[menorIndex];
        vc[menorIndex] = vc[i];
        vc[i] = aux;
      }
    } else {
      return 0;
    }
    return 3;
  }

  // + ------------------------------------------------------------- +
  // + SHELL SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int shellSort(String algorithm) {
    int divider = 1, j = 0, length = this.numContatos;
    Pessoa aux;
    if (algorithm.equals("age")) {
      while (divider < length) {
        divider *= 3 + 1;
      }
      divider /= 3;
      while (divider > 0) {
        for (int i = divider; i < length; i++) {
          aux = vc[i];
          j = i;
          while (j >= divider && vc[j - divider].getIdade() > aux.getIdade()) {
            vc[j] = vc[j - divider];
            j -= divider;
          }
          vc[j] = aux;
        }
        divider /= 2;
      }
    } else if (algorithm.equals("name")) {
      while (divider < length) {
        divider *= 3 + 1;
      }
      divider /= 3;
      while (divider > 0) {
        for (int i = divider; i < length; i++) {
          aux = vc[i];
          j = i;
          while (j >= divider && vc[j - divider].getNome().compareTo(aux.getNome()) > 0) {
            vc[j] = vc[j - divider];
            j -= divider;
          }
          vc[j] = aux;
        }
        divider /= 2;
      }
    } else if (algorithm.equals("surname")) {
      while (divider < length) {
        divider *= 3 + 1;
      }
      divider /= 3;
      while (divider > 0) {
        for (int i = divider; i < length; i++) {
          aux = vc[i];
          j = i;
          while (j >= divider && vc[j - divider].getSobrenome().compareTo(aux.getSobrenome()) > 0) {
            vc[j] = vc[j - divider];
            j -= divider;
          }
          vc[j] = aux;
        }
        divider /= 2;
      }
    } else {
      return 0;
    }
    return 4;
  }

  void shellSortK(Pessoa array[], int len, String algorithm) {
    for (int gap = len / 2; gap > 0; gap /= 2) {
      for (int i = gap; i < len; i += 1) {
        Pessoa temp = array[i];
        int j;
        for (j = i; j >= gap && array[j - gap].getIdade() > temp.getIdade(); j -= gap) {
          array[j] = array[j - gap];
        }
        array[j] = temp;
      }
    }
  }

  // + ------------------------------------------------------------- +
  // + QUICK SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int QuickSort(Pessoa[] v, int leftElement, int rightElement, String algorithm) {
    int left = leftElement;
    int right = rightElement;
    Pessoa pivot = v[(left + right) / 2], aux;

    if (algorithm.equals("age")) {
      while (left <= right) {
        while (v[left].getIdade() < pivot.getIdade()) {
          left = left + 1;
        }
        while (v[right].getIdade() > pivot.getIdade()) {
          right = right - 1;
        }
        if (left <= right) {
          aux = v[left];
          v[left] = v[right];
          v[right] = aux;
          left = left + 1;
          right = right - 1;
        }
      }

    } else if (algorithm.equals("surname")) {
      while (left <= right) {
        while (((v[left].getSobrenome().compareTo(pivot.getSobrenome())) < 0)) {
          left = left + 1;
        }
        while (((v[right].getSobrenome().compareTo(pivot.getSobrenome())) > 0)) {
          right = right - 1;
        }
        if (left <= right) {
          aux = v[left];
          v[left] = v[right];
          v[right] = aux;
          left = left + 1;
          right = right - 1;
        }
      }

    } else if (algorithm.equals("name")) {
      while (left <= right) {
        while (((v[left].getNome().compareTo(pivot.getNome())) < 0)) {
          left = left + 1;
        }
        while (((v[right].getNome().compareTo(pivot.getNome())) > 0)) {
          right = right - 1;
        }
        if (left <= right) {
          aux = v[left];
          v[left] = v[right];
          v[right] = aux;
          left = left + 1;
          right = right - 1;
        }
      }
    } else {
      return 0;
    }
    if (right > leftElement) {
      QuickSort(v, leftElement, right, algorithm);
    }
    if (left < rightElement) {
      QuickSort(v, left, rightElement, algorithm);
    }
    return 5;
  }

  // + ------------------------------------------------------------- +
  // + HEAP SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int heapify(Pessoa[] person, int heapLen, int i, String algorithm) {
    Pessoa swap;
    int leftIdx = 2 * i + 1, rightIdx = 2 * i + 2, largest = i;
    if (algorithm.equals("age")) {
      if (leftIdx < heapLen && person[leftIdx].getIdade() > person[largest].getIdade()) {
        largest = leftIdx;
      }
      if (rightIdx < heapLen && person[rightIdx].getIdade() > person[largest].getIdade()) {
        largest = rightIdx;
      }
      if (largest != i) {
        swap = person[i];
        person[i] = person[largest];
        person[largest] = swap;
        heapify(person, heapLen, largest, algorithm);
      }
    } else if (algorithm.equals("name")) {
      if (leftIdx < heapLen && person[leftIdx].getNome().compareTo(person[largest].getNome()) > 0) {
        largest = leftIdx;
      }
      if (rightIdx < heapLen && person[rightIdx].getNome().compareTo(person[largest].getNome()) > 0) {
        largest = rightIdx;
      }
      if (largest != i) {
        swap = person[i];
        person[i] = person[largest];
        person[largest] = swap;
        heapify(person, heapLen, largest, algorithm);
      }
    } else if (algorithm.equals("surname")) {
      if (leftIdx < heapLen && person[leftIdx].getSobrenome().compareTo(person[largest].getSobrenome()) > 0) {
        largest = leftIdx;
      }
      if (rightIdx < heapLen && person[rightIdx].getSobrenome().compareTo(person[largest].getSobrenome()) > 0) {
        largest = rightIdx;
      }
      if (largest != i) {
        Pessoa temp = person[i];
        person[i] = person[largest];
        person[largest] = temp;
        heapify(person, heapLen, largest, algorithm);
      }
    } else {
      return 0;
    }
    return 6;
  }

  public int heapSort(String algorithm) {
    int length = this.numContatos, i = 0, r = 0;
    for (i = length / 2 - 1; i >= 0; i--) { // encontrar o max heap
      r = heapify(vc, length, i, algorithm);
    }
    for (i = length - 1; i >= 0; i--) { // Heap sort
      Pessoa temp = vc[0];
      vc[0] = vc[i];
      vc[i] = temp;
      r = heapify(vc, i, 0, algorithm); // Heapify root elemento
    }
    return r;
  }

  // + ------------------------------------------------------------- +
  // + MERGE SORT ALGORITHM +
  // + ------------------------------------------------------------- +
  public int merge(int p, int q, int right, String typeOrder) {
    int n1 = q - p + 1;
    int n2 = right - q;
    Pessoa L[] = new Pessoa[n1];
    Pessoa M[] = new Pessoa[n2];

    if (typeOrder.equals("age")) {
      for (int i = 0; i < n1; i++)
        L[i] = this.vc[p + i];
      for (int j = 0; j < n2; j++)
        M[j] = this.vc[q + 1 + j];
      int i = 0, j = 0, k = p;
      while (i < n1 && j < n2) {
        if (L[i].getIdade() <= M[j].getIdade()) {
          this.vc[k] = L[i];
          i++;
        } else {
          this.vc[k] = M[j];
          j++;
        }
        k++;
      }
      while (i < n1) {
        this.vc[k] = L[i];
        i++;
        k++;
      }
      while (j < n2) {
        this.vc[k] = M[j];
        j++;
        k++;
      }
    } else if (typeOrder.equals("name")) {
      for (int i = 0; i < n1; i++)
        L[i] = this.vc[p + i];
      for (int j = 0; j < n2; j++)
        M[j] = this.vc[q + 1 + j];
      int i = 0, j = 0, k = p;
      while (i < n1 && j < n2) {
        if (L[i].getNome().compareTo(M[j].getNome()) < 0) {
          this.vc[k] = L[i];
          i++;
        } else {
          this.vc[k] = M[j];
          j++;
        }
        k++;
      }
      while (i < n1) {
        this.vc[k] = L[i];
        i++;
        k++;
      }
      while (j < n2) {
        this.vc[k] = M[j];
        j++;
        k++;
      }
    } else if (typeOrder.equals("surname")) {
      for (int i = 0; i < n1; i++)
        L[i] = this.vc[p + i];
      for (int j = 0; j < n2; j++)
        M[j] = this.vc[q + 1 + j];
      int i = 0, j = 0, k = p;
      while (i < n1 && j < n2) {
        if (L[i].getSobrenome().compareTo(M[j].getSobrenome()) < 0) {
          this.vc[k] = L[i];
          i++;
        } else {
          this.vc[k] = M[j];
          j++;
        }
        k++;
      }
      while (i < n1) {
        this.vc[k] = L[i];
        i++;
        k++;
      }
      while (j < n2) {
        this.vc[k] = M[j];
        j++;
        k++;
      }
    } else {
      return 0;
    }
    return 7;
  }

  public int mergeSort(int left, int right, String typeOrder) {
    int r = 0;
    if (left < right) {
      int merge = (left + right) / 2;
      mergeSort(left, merge, typeOrder);
      mergeSort(merge + 1, right, typeOrder);
      r = merge(left, merge, right, typeOrder);
    }
    return r;
  }
}

/*
 * O algoritmo passa várias vezes pela lista dividindo o grupo maior em menores
 * Nos grupos menores é aplicado o método da ordenação por inserção.
 */
