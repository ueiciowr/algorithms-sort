package com.person;

import java.util.Scanner;

import com.time.Time;

public class OrdenaObjetoApp {
  public static void main(String[] args) throws Exception {
    int arrSize = 20;

    Contatos persons = new Contatos(arrSize);
    Time time = new Time();
    Gerador g = new Gerador();
    int identifier = 1;

    for (int i = 0; i < arrSize; i++) {
      persons.inserePessoa(identifier, g.geraNome(), g.geraSobrenome(), g.geraIdade());
      identifier++;
    }

    String formatString = "-----------------------------------";
    System.out.print(formatString + "\nQual algoritmo você deseja executar ?\n" + formatString
        + "\n0) BubbleSort adaptado\n1) BubbleSort\n2) SelectionSort\n3) InsertionSort\n4) ShellSort\n5) QuickSort\n6) HeapSort\n7) ShellSortK\n8) MergeSort"
        + "\n\nInsira o número correspondente: ");

    Scanner scan = new Scanner(System.in);
    int key = scan.nextInt();
    System.out.print(formatString + "\nVocê escolheu a opção " + key);
    System.out.print("\n\nPor qual forma deseja ordenar ?\n'name'\n'surname'\n'age'\n'id'\nOpção: ");
    String typeOfOrder = scan.next();
    scan.close();

    long initialTime = System.currentTimeMillis();
    int result = 0;
    switch (key) {
      case 0:
        persons.bubbleSortFirstNameAdapted();
        break;
      case 1:
        result = persons.bubbleSort(typeOfOrder);
        break;
      case 2:
        result = persons.selectionSort(typeOfOrder);
        break;
      case 3:
        result = persons.insertionSort(typeOfOrder);
        break;
      case 4:
        result = persons.shellSort(typeOfOrder);
        break;
      case 5:
        result = persons.QuickSort(persons.getPerson(), 0, persons.getNumContatos() - 1, typeOfOrder);
        break;
      case 6:
        result = persons.heapSort(typeOfOrder);
        break;
      case 7:
        persons.shellSortK(persons.getPerson(), persons.getNumContatos(), typeOfOrder);
        break;
      case 8:
        result = persons.mergeSort(0, persons.getNumContatos() - 1, typeOfOrder);
        break;
      default:
        break;
    }
    long finalTime = System.currentTimeMillis();
    time.setTime(finalTime, initialTime);
    persons.imprimeContatos(time.getTime(), result);
  }
}
